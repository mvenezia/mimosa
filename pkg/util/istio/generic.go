package istio

import (
	"github.com/juju/loggo"
	logutil "gitlab.com/mvenezia/log-util/pkg/log"
	k8sutil "gitlab.com/mvenezia/mimosa/pkg/util/k8s"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

var (
	logger loggo.Logger
)

func PrepareRestClient(config *rest.Config) (dynamic.Interface, error) {
	if config == nil {
		config = k8sutil.DefaultConfig
	}

	return dynamic.NewForConfig(config)
}

func SetLogger() {
	logger = logutil.GetModuleLogger("pkg.util.ccutil", loggo.INFO)
}
