package istio

import (
	"fmt"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	_ "k8s.io/client-go/plugin/pkg/client/auth"
)

func DoSomething(client dynamic.Interface) {
	resource := schema.GroupVersionResource{Group: "networking.istio.io", Version: "v1alpha3", Resource: "serviceentries"}

	fmt.Printf("Listing serviceentries:\n")
	list, _ := client.Resource(resource).Namespace("").List(metav1.ListOptions{})
	for _, d := range list.Items {
		fmt.Printf(" * %s (%s)\n", d.GetName(), d.GetNamespace())
		addresses, found, err := unstructured.NestedSlice(d.Object, "spec", "addresses")
		if err == nil && found == true {
			fmt.Printf("\tIP: %s\n", addresses[0])
		}
		hostnames, found, err := unstructured.NestedSlice(d.Object, "spec", "hosts")
		if err == nil && found == true {
			fmt.Printf("\tHost: %s\n", hostnames[0])
		}
		endpoints, found, err := unstructured.NestedSlice(d.Object, "spec", "endpoints")
		if err == nil && found == true {
			endpointEntry := endpoints[0].(map[string]interface{})
			fmt.Printf("\tEndpoint: %s\n", endpointEntry["address"])
		}
	}

}
