package ipdealer

import (
	"fmt"
	"net"
)

type MemoryDealer struct {
	reservations map[string]int
	cidr         string
	firstIP      net.IP
	lastIP       net.IP
	currentIP    net.IP
}

func NewMemoryDealer(cidr string) (Dealer, error) {
	first, current, last, err := GetCurrentMinMaxIPsForCIDR(cidr)
	if err != nil {
		return nil, err
	}

	return &MemoryDealer{
		reservations: make(map[string]int),
		cidr: cidr,
		firstIP: net.IPv4(first[0], first[1], first[2], first[3]),
		currentIP: net.IPv4(current[0], current[1], current[2], current[3]),
		lastIP: net.IPv4(last[0], last[1], last[2], last[3]),
	}, nil
}

func (d *MemoryDealer) SetCIDRMask(cidr string) error {
	first, current, last, err := GetCurrentMinMaxIPsForCIDR(cidr)
	if err != nil {
		return err
	}

	d.firstIP = net.IPv4(first[0], first[1], first[2], first[3])
	d.currentIP = net.IPv4(current[0], current[1], current[2], current[3])
	d.lastIP = net.IPv4(last[0], last[1], last[2], last[3])
	d.cidr = cidr

	return nil
}

func (d *MemoryDealer) GetCIDRMask() string {
	return d.cidr
}

func (d *MemoryDealer) ReserveIP(ip net.IP) (int, error) {
	value, ok := d.reservations[ip.String()]
	if ok && value < 0 {
		// weird, somehow the count was < 0?
		d.reservations[ip.String()] = 1
	} else if value > 0 {
		d.reservations[ip.String()] = 1
		return d.reservations[ip.String()], fmt.Errorf("this ip has already been reserved")
	} else {
		d.reservations[ip.String()]++
	}

	return d.reservations[ip.String()], nil
}

func (d *MemoryDealer) ReleaseIP(ip net.IP) (int, error) {
	value, _ := d.reservations[ip.String()]
	if value <= 0 {
		// weird, somehow the count was < 0?
		d.reservations[ip.String()] = 0
		return 0, fmt.Errorf("was asked to release an IP that I never had")
	} else {
		d.reservations[ip.String()]--
	}
	return d.reservations[ip.String()], nil
}

func (d *MemoryDealer) RequestIP() (net.IP, error) {
	output := make(net.IP, 4)
	startingCurrentIP := make(net.IP, 4)
	for i := 0; i < 4; i++ {
		startingCurrentIP[i] = d.currentIP.To4()[i]
	}

	for {
		// Let's be optimistic...
		if d.reservations[d.currentIP.String()] == 0 {
			for i := 0; i < 4; i++ {
				output[i] = d.currentIP.To4()[i]
			}
			d.reservations[d.currentIP.String()]++
			return output, nil
		}

		d.currentIP = IncrementIP(d.currentIP)
		if d.currentIP.Equal(d.lastIP) {
			for i := 0; i < 4; i++ {
				d.currentIP[i] = d.firstIP[i]
			}
		}

		if startingCurrentIP.Equal(d.currentIP) {
			return output, fmt.Errorf("Could not find an available address - looped around!")
		}
	}
}

func (d *MemoryDealer) FlushDatabase() error {
	d.reservations = make(map[string]int)
	first, current, last, err := GetCurrentMinMaxIPsForCIDR(d.cidr)
	if err != nil {
		return err
	}

	d.firstIP = net.IPv4(first[0], first[1], first[2], first[3])
	d.currentIP = net.IPv4(current[0], current[1], current[2], current[3])
	d.lastIP = net.IPv4(last[0], last[1], last[2], last[3])
	return nil
}

func (d *MemoryDealer) ExportDatabase() map[string]int {
	output := make(map[string]int)
	for i, j := range d.reservations {
		output[i] = j
	}

	return output
}

func GetCurrentMinMaxIPsForCIDR(cidr string) (first net.IP, current net.IP, last net.IP, err error) {
	first = make(net.IP, 4)
	current = make(net.IP, 4)
	last = make(net.IP, 4)

	_, network, err := net.ParseCIDR(cidr)
	// Could not parse the provided CIDR
	if err != nil {
		return
	}

	for i := 0; i < 4; i++ {
		first[i] = network.IP[i]
		current[i] = network.IP[i]
		last[i] = network.IP[i] + (network.Mask[i] ^ 255)
	}

	return
}

func IncrementIP(ip net.IP) net.IP {
	i := ip.To4()
	v := uint(i[0])<<24 + uint(i[1])<<16 + uint(i[2])<<8 + uint(i[3])
	v++
	fourth := byte(v & 0xFF)
	third := byte((v >> 8) & 0xFF)
	second := byte((v >> 16) & 0xFF)
	first := byte((v >> 24) & 0xFF)
	return net.IPv4(first, second, third, fourth)
}
