package ipdealer_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/mvenezia/mimosa/pkg/ipdealer"
	"net"
)

var _ = Describe("IPDealer Memory Dealer Functions", func() {
	Context("when working with a /16 cidr", func() {
		var (
			dealer ipdealer.Dealer
		)
		BeforeEach(func() {
			dealer, _ = ipdealer.NewMemoryDealer("127.255.0.0/16")
		})
		Context("reserving an address", func() {
			Context("when it isn't already reserved", func() {
				var (
					number int
					err error
				)
				BeforeEach(func() {
					number, err = dealer.ReserveIP(net.ParseIP("127.255.0.1"))
				})
				It("no error should occur and the reservation count should be 1", func() {
					Expect(err).NotTo(HaveOccurred())
					Expect(number).Should(Equal(1))
				})
			})
			Context("when it is already reserved", func() {
				var (
					number int
					err error
				)
				BeforeEach(func() {
					_, _ = dealer.ReserveIP(net.ParseIP("127.255.0.1"))
					number, err = dealer.ReserveIP(net.ParseIP("127.255.0.1"))
				})
				It("an error should occur and the reservation count should be 1", func() {
					Expect(err).To(HaveOccurred())
					Expect(number).Should(Equal(1))
				})
			})
		})
		Context("releasing an address", func() {
			Context("when it isn't already reserved", func() {
				var (
					number int
					err error
				)
				BeforeEach(func() {
					number, err = dealer.ReleaseIP(net.ParseIP("127.255.0.1"))
				})
				It("an error should occur and the reservation count should be 0", func() {
					Expect(err).To(HaveOccurred())
					Expect(number).Should(Equal(0))
				})
			})
			Context("when it is already reserved", func() {
				var (
					number int
					err error
				)
				BeforeEach(func() {
					_, _ = dealer.ReserveIP(net.ParseIP("127.255.0.1"))
					number, err = dealer.ReleaseIP(net.ParseIP("127.255.0.1"))
				})
				It("no error should occur and the reservation count should be 0", func() {
					Expect(err).NotTo(HaveOccurred())
					Expect(number).Should(Equal(0))
				})
			})
		})
		Context("requesting an address", func() {
			Context("first address", func() {
				var (
					ip net.IP
					err error
				)
				BeforeEach(func() {
					ip, err = dealer.RequestIP()
				})
				It("no error should occur and the ip address should be 127.255.0.0", func() {
					Expect(err).NotTo(HaveOccurred())
					Expect(ip.String()).Should(Equal("127.255.0.0"))
				})
			})
			Context("second address", func() {
				var (
					ip net.IP
					err error
				)
				BeforeEach(func() {
					_, _ = dealer.RequestIP()
					ip, err = dealer.RequestIP()
				})
				It("no error should occur and the ip address should be 127.255.0.1", func() {
					Expect(err).NotTo(HaveOccurred())
					Expect(ip.String()).Should(Equal("127.255.0.1"))
				})
			})
		})
	})
})
