package ipdealer_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestIPDealer(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "IPDealer Suite")
}
