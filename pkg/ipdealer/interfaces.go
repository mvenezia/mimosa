package ipdealer

import "net"

type Dealer interface {
	SetCIDRMask(string) error
	GetCIDRMask() string
	ReserveIP(ip net.IP) (int, error)
	ReleaseIP(ip net.IP) (int, error)
	RequestIP() (net.IP, error)
	ExportDatabase() map[string]int
	FlushDatabase() error
}
