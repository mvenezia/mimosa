module gitlab.com/mvenezia/mimosa

go 1.12

require (
	github.com/juju/loggo v0.0.0-20190212223446-d976af380377
	github.com/onsi/ginkgo v1.6.0
	github.com/onsi/gomega v0.0.0-20190113212917-5533ce8a0da3
	github.com/spf13/cobra v0.0.4
	github.com/spf13/viper v1.4.0
	gitlab.com/mvenezia/log-util v0.0.0-20190320033643-1d7f7048b464
	gitlab.com/mvenezia/version-info v0.0.0-20190320033107-d83c50c7bd4e
	k8s.io/api v0.0.0-20190515023547-db5a9d1c40eb
	k8s.io/apimachinery v0.0.0-20190515023456-b74e4c97951f
	k8s.io/client-go v0.0.0-20190515063710-7b18d6600f6b
	k8s.io/klog v0.3.0
)
