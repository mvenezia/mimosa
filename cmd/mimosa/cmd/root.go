package cmd

import (
	"flag"
	"fmt"
	"github.com/juju/loggo"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	logutil "gitlab.com/mvenezia/log-util/pkg/log"
	"gitlab.com/mvenezia/mimosa/pkg/util/istio"
	k8s "gitlab.com/mvenezia/mimosa/pkg/util/k8s"
	"os"
	"strings"
)

var (
	rootCmd = &cobra.Command{
		Use:   "mimosa",
		Short: "mimosa cli",
		Long:  `Mike's Istio Mesh Operations Shell Application'`,
		Run: func(cmd *cobra.Command, args []string) {
			doStuff()
		},
	}
)

func init() {
	viper.SetEnvPrefix("mimosa")
	replacer := strings.NewReplacer("-", "_")
	viper.SetEnvKeyReplacer(replacer)

	// using standard library "flag" package
	rootCmd.PersistentFlags().String("kubeconfig", "", "Location of kubeconfig file")

	viper.BindPFlag("kubeconfig", rootCmd.PersistentFlags().Lookup("kubeconfig"))

	viper.AutomaticEnv()
	rootCmd.Flags().AddGoFlagSet(flag.CommandLine)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func doStuff() {
	_ = logutil.GetModuleLogger("cmd.mimosa", loggo.INFO)

	// get flags
	_ = viper.GetInt("port")

	fmt.Println("Hi Mom")
	config, _ := k8s.GenerateKubernetesConfig()
	client, _ := istio.PrepareRestClient(config)
	//istio.DoSomething(client)
	istio.DoSomething2(client)
}
